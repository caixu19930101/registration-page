function fibonacci(index) {
    const init = [1];

    function convert(arg) {
        if (Object.is(Number(arg), NaN)) {
            return 0
        }
        return Number(arg)
    }

        for (let i = 1; i < index - 1; i++) {
             let len = init.length;
             init.push(convert(init[len - 2]) + convert(init[len - 1]))
         }
    return init
}

console.log(fibonacci(6));
